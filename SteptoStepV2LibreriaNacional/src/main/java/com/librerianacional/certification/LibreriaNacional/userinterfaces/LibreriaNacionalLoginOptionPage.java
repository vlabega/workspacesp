package com.librerianacional.certification.LibreriaNacional.userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LibreriaNacionalLoginOptionPage extends PageObject {

	public static final Target BEGIN_SESION = Target.the("begin Sesion button").locatedBy(
			"//div[@class='d-flex col-11 col-sm-11 col-md-12 col-lg-10 col-xl-6 mx-auto']//div[@class='col-12 px-0 mb-md-3']//div[@class='d-flex justify-content-between align-content-center border px-2 px-md-3 py-3 shadow-bx']");
}
