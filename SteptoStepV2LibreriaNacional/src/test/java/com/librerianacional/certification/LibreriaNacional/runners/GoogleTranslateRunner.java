package com.librerianacional.certification.LibreriaNacional.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/feature/google_translate.feature", 
		glue = "com.librerianacional.certification.LibreriaNacional.stepdefinitions"
)


public class GoogleTranslateRunner {

}
